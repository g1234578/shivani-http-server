const fs = require("fs").promises;
const http = require("http");
const uuid = require("uuid");

const server = http.createServer(async (request, response) => {
  try {
    const htmlContent = await fs.readFile("./public/index.html", "utf-8");
    const jsonData = await fs.readFile("./public/data.json", "utf-8");
    let pathParts = request.url.split("/");

    if (request.method === "GET" && pathParts[1] == "html") {
      response.writeHead(200, { "Content-Type": "text/html" });
      response.end(htmlContent);
    } else if (request.method === "GET" && pathParts[1] === "json") {
      response.writeHead(200, { "Content-Type": "application/json" });
      response.end(jsonData);
    } else if (request.method === "GET" && pathParts[1] === "uuid") {
      const uuidValue = uuid.v4();
      response.writeHead(200, { "Content-Type": "application/json" });
      response.end(JSON.stringify({ uuid: uuidValue }), null, 4);
    } else if (
      request.method === "GET" &&
      pathParts[1] == "status" &&
      pathParts[2]
    ) {
      const statusCode = parseInt(pathParts[2], 10);
      if (!isNaN(statusCode) && statusCode >= 100 && statusCode < 600) {
        response.writeHead(statusCode, { "Content-Type": "text/plain" });
        response.end(`Status code: ${statusCode}`);
      } else {
        response.writeHead(400, { "Content-Type": "text/plain" });
        response.end("Invalid status code");
      }
    } else if (
      request.method === "GET" &&
      pathParts[1] == "delay" &&
      pathParts[2]
    ) {
      const timeDelay = parseInt(pathParts[2], 10);
      if (!isNaN(timeDelay)) {
        setTimeout(() => {
          response.writeHead(200, { "Content-Type": "text/plain" });
          response.end(`Success after ${timeDelay} seconds`);
        }, timeDelay * 1000);
      }
    } else {
      response.writeHead(404, { "Content-Type": "text/plain" });
      response.end("Not Found");
    }
  } catch (error) {
    console.log(error.message);
  }
});

server.listen(3000, () => {
  console.log("Server running at http://localhost:3000/");
});
